Feature:  Hotel booking feature
  Scenario:  Book this Now button visible
    Given I navigate to the MakeMyTrip Website
    When I am viewing the ‘Home’ page Then I am presented with banner where Flight, Hotels, Homestays are mentioned
    Then I click on “Hotels”
    And I select city kolkata
    And I select Check in Date as 23rd Dec 2023 and Check out Date as 25th Dec 2023
    And I select 2 Rooms with 4 Adults and 1 Child
    And I select age of Children as 11 years
    Then I click on Search button
    Then I verify Search filter section and total number of properties available
    And I select MMT Luxe selection from search filter available in left pane
    Then I Click on Taj Bengal, Kolkata
    Then I verify a new Tab will open with Taj Bengal Hotel booking option
    Then I verify “BOOK THIS NOW” button



package runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = "src/test/resources/features",
        glue = "steps",
        dryRun = false,
        monochrome = true,
        plugin = {"pretty",
        "html:target/cucumber.html",

        "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"}

)

public class TestRunner extends AbstractTestNGCucumberTests {


}


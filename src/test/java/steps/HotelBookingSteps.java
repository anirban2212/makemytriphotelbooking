package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import pages.actions.HomePageActions;
import pages.actions.HotelPageActions;
import pages.actions.SearchResultPageActions;
import utils.DriverManager;
import java.io.IOException;

public class HotelBookingSteps {

    @Given("I navigate to the MakeMyTrip Website")
    public void i_navigate_to_the_make_my_trip_website() throws IOException, InterruptedException {
        DriverManager.setUpDriver();
        HomePageActions.getHomePageTitleTest();

    }

    @When("I am viewing the ‘Home’ page Then I am presented with banner where Flight, Hotels, Homestays are mentioned")
    public void i_am_viewing_the_home_page_then_i_am_presented_with_banner_where_flight_hotels_homestays_are_mentioned() {

        Assert.assertTrue(HomePageActions.isFlightBannerPresent(),
                "FlightBanner is not displayed in homepage");
        DriverManager.log.info("----FlightBanner is displayed in homepage----");
        Assert.assertTrue(HomePageActions.isHomestaysPresent(),
                "HomeStaysBanner is not displayed in homepage");
        DriverManager.log.info("----HomeStaysBanner is displayed in homepage----");
        Assert.assertTrue(HomePageActions.isHotelsBannerPresent(),
                "HotelsBanner is not displayed in homepage");
        DriverManager.log.info("----HotelsBanner is displayed in homepage----");

    }

    @Then("I click on “Hotels”")
    public void i_click_on_hotels() {
        HomePageActions.navigateToHotelsPage();
        DriverManager.log.info("----Navigated to Hotels Page----");
        Assert.assertEquals("MakeMyTrip.com: Save upto 60% on Hotel Booking 4,442,00+ Hotels Worldwide",
                HotelPageActions.getHotelPageTitle(), "Hotels page Title is different");


    }

    @Then("^I select city (.+)$")
    public void i_select_city_as_kolkata(String cityName) throws InterruptedException {
        HotelPageActions.selectCity(cityName);
        DriverManager.log.info("---cityName entered successfully----");
    }

    @Then("I select Check in Date as 23rd Dec 2023 and Check out Date as 25th Dec 2023")
    public void i_select_check_in_date_as_23rd_dec_and_check_out_date_as_25th_dec() {


        HotelPageActions.selectCheckInDate();
        DriverManager.log.info("--CheckInDate picked Sucessfully--");
       HotelPageActions.selectCheckOutDate();
        DriverManager.log.info("--checkoutDate picked Sucessfully----");


    }

    @Then("I select 2 Rooms with 4 Adults and 1 Child")
    public void i_select_rooms_with_adults_and_child() {
        HotelPageActions.selectRoom();
        DriverManager.log.info("----2 rooms selected successfully----");
        HotelPageActions.selectNoOfAdults();
        DriverManager.log.info("--4 adults selected ---");
        HotelPageActions.selectNoOfChildren();
        DriverManager.log.info("---1 child selected succfully--");

    }

    @Then("I select age of Children as 11 years")
    public void i_select_age_of_children_as_years() {
        HotelPageActions.selectChildAge();

    }

    @Then("I click on Search button")
    public void i_click_on_search_button() {

        HotelPageActions.clickOnApplyBtnAndSearch();


    }

    @Then("I verify Search filter section and total number of properties available")
    public void i_verify_search_filter_section_and_total_number_of_properties_available() throws InterruptedException {

        SearchResultPageActions.verifySearchFilterAndProperies();
    }

    @Then("I select MMT Luxe selection from search filter available in left pane")
    public void i_select_mmt_luxe_selection_from_search_filter_available_in_left_pane() {
        SearchResultPageActions.selectMMTLuxe();
    }

    @Then("I Click on Taj Bengal, Kolkata")
    public void i_click_on_taj_bengal_kolkata() throws InterruptedException {
        SearchResultPageActions.clickOnTajBengal();
    }

    @Then("I verify a new Tab will open with Taj Bengal Hotel booking option")
    public void i_verify_a_new_tab_will_open_with_taj_bengal_hotel_booking_option() throws InterruptedException {
        SearchResultPageActions.verifyTajBengalBookingPage();

    }

    @Then("I verify “BOOK THIS NOW” button")
    public void i_verify_book_this_now_button() {
        SearchResultPageActions.verifyBookThisNowButton();

    }


}

package utils;

import io.cucumber.java.After;
import io.github.bonigarcia.wdm.WebDriverManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;

import java.io.IOException;


public class DriverManager {

    public static Logger log = LogManager.getLogger(DriverManager.class);
    public static Constants constants;
    public static WebDriver driver;

    public DriverManager() {

    }

    public static void setUpDriver() throws IOException {
        constants = new Constants();
        if (constants.getBrowserName().equalsIgnoreCase("chrome")) {
            String driverPath = System.getProperty("user.dir")
                    + "//src//test//resources//chromedriver.exe";
            System.setProperty("webdriver.chrome.driver", driverPath);
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--remote-allow-origins=*");
            driver = new ChromeDriver(chromeOptions);


        } else if (constants.getBrowserName().equalsIgnoreCase("edge")) {
            WebDriverManager.edgedriver().setup();
            driver = new EdgeDriver();

        }


        driver.get(constants.getUrl());
        log.info("-----Chrome Browser Initialized------");

        BaseUtils.maximizeWindow(driver);
        BaseUtils.pageLoadTimeOut(driver);
        BaseUtils.clearCookies(driver);
        BaseUtils.implicitWait(driver);
        BaseUtils.zoomOut();
        BaseUtils.closeIframePopUp(driver);


    }

    /*
     * used for returning the current instance of
     *     the webdriver.
     */


    public static WebDriver getDriver() {
        return driver;
    }

    @After
    public static void tearDown() {
        driver.quit();
        log.info("---ChromeDriver closed sucessfully-----");
    }


}

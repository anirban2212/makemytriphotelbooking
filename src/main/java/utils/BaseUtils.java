package utils;

import io.cucumber.java.AfterStep;
import io.cucumber.java.Scenario;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.actions.HomePageActions;
import pages.actions.HotelPageActions;
import pages.actions.SearchResultPageActions;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BaseUtils {
    public static WebDriverWait wait;
    public static HomePageActions homePageActions = new HomePageActions(DriverManager.getDriver());
    public static HotelPageActions hotelPageActions = new HotelPageActions(DriverManager.getDriver());
    public static SearchResultPageActions searchResultPageActions = new SearchResultPageActions(DriverManager.getDriver());
    public BaseUtils() {
    }

    public static void maximizeWindow(WebDriver driver) {
        driver.manage().window().maximize();
    }

    public static void implicitWait(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    public static void pageLoadTimeOut(WebDriver driver) {
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        //WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(10));
    }

    public static void explicitWait(WebElement element, WebDriver driver) {
        wait = new WebDriverWait(driver, Duration.ofSeconds(40));
        wait.until(ExpectedConditions.visibilityOf(element));

    }

    public static void explicitWaitClick(WebElement element, WebDriver driver) {
        wait = new WebDriverWait(driver, Duration.ofSeconds(40));
        wait.until(ExpectedConditions.elementToBeClickable(element));

    }

    public static void zoomOut() {
        try {
            Robot robot = new Robot();
            for (int i = 0; i < 2; i++) {
                robot.keyPress(KeyEvent.VK_CONTROL);
                robot.keyPress(KeyEvent.VK_SUBTRACT);
                robot.keyRelease(KeyEvent.VK_SUBTRACT);
                robot.keyRelease(KeyEvent.VK_CONTROL);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }


    }

    public static void clearCookies(WebDriver driver) {
        driver.manage().deleteAllCookies();
    }

    public static void closeIframePopUp(WebDriver driver) {
        List<WebElement> iframe = driver.findElements(By.tagName("iframe"));
        if (iframe.size() > 2) {
            WebElement notificationIframe = driver.findElement(By.xpath("//iframe[@title='notification-frame-317738b3']"));
            explicitWait(notificationIframe, driver);
            driver.switchTo().frame("webklipper-publisher-widget-container-notification-frame");
            driver.findElement(By.xpath("//a[@class='close']")).click();
            DriverManager.log.info("---popup closed sucessfully---");
            driver.switchTo().defaultContent();
        } else {
            DriverManager.log.info("---No PopUp present--");
        }
    }
    @AfterStep
    public void AddScreenShot(Scenario scenario) throws IOException {
        if(scenario.isFailed()){
            File sourcePath=((TakesScreenshot)DriverManager.driver).getScreenshotAs(OutputType.FILE);
            byte[] fileContent=FileUtils.readFileToByteArray(sourcePath);
            scenario.attach(fileContent,"image/png","image");
        }
    }
}

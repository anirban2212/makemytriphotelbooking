package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePageLocators {
    @FindBy(xpath = "//a[@href='https://www.makemytrip.com/flights/']")
    public WebElement flightBanner;

    @FindBy(xpath = "//a[@href='https://www.makemytrip.com/hotels/']")
    public WebElement hotelsBanner;

    @FindBy(xpath = "//a[@href='https://www.makemytrip.com/homestays/']")
    public WebElement homeStaysBanner;


}

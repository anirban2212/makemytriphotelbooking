package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class HotelPageLocators {

    @FindBy(xpath = "//input[@id='city' and @type='text']")
    public WebElement cityField;

    @FindBy(xpath = "//input[@type='text' and @placeholder='Where do you want to stay?']")
    public WebElement cityTextBox;
    @FindBy(xpath = "(//div[@class='clickable'])[1]")
    public WebElement firstSuggesation;
    @FindBy(xpath = "(//div[@class='DayPicker-Caption']//div)[1]")
    public WebElement monthYear;
    @FindBy(xpath = "(//div[@class='DayPicker-Caption']//div//span)[1]")
    public WebElement year;

    @FindBy(xpath = "//span[@role='button' and @class='DayPicker-NavButton DayPicker-NavButton--next']")
    public WebElement nextBtnCal;
    @FindBy(xpath = "(//div[@class='DayPicker-Body'])[1]//div[@class='DayPicker-Week']//div[@class='DayPicker-Day' or @class='DayPicker-Day DayPicker-Day--today' or @class='DayPicker-Day DayPicker-Day--start DayPicker-Day--selected' or @class='DayPicker-Day DayPicker-Day--end DayPicker-Day--selected']")
    public List<WebElement> checkInDate;

    @FindBy(xpath = "(//div[@class='DayPicker-Body'])[1]//div[@class='DayPicker-Week']//div[@class='DayPicker-Day' or @ class='DayPicker-Day DayPicker-Day--selected']")
    public List<WebElement> checkOutDate;
    @FindBy(xpath = "(//p[text()='Rooms']/ancestor::div/following::div//div)[1]")
    public WebElement roomsBtn;
    @FindBy(xpath = "//*[@class=\"gstSlct__list\"]/li[2]")
    public WebElement roomNo;
    @FindBy(xpath = "(//p[text()='Adults']/ancestor::div/following::div//div)[1]")
    public WebElement adultsBtn;
    @FindBy(xpath = "//ul[@class='gstSlct__list']//li[3]")
    public WebElement noOfAdults;

    @FindBy(xpath = "(//p[text()='Children']/ancestor::div/following::div//div)[1]")
    public WebElement childernBtn;
    @FindBy(xpath = "//ul[@class='gstSlct__list']//li[2]")
    public WebElement noOfChild;
    @FindBy(xpath = "//*[@data-testid='child_count']")
    public WebElement childBtn;
    @FindBy(xpath = "//ul[@class='gstSlct__list']//li[12]")
    public WebElement childAge;
    @FindBy(xpath = "//button[contains(text(),'Apply')]")
    public WebElement applyBtn;
    @FindBy(xpath = "//button[contains(text(),'Search')]")
    public WebElement searchBtn;


}

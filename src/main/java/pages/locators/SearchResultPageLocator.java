package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultPageLocator {
    @FindBy(xpath = "//div[@class=\"filterWrapNew appendRight30\"]")
    public WebElement filterField;
    @FindBy(xpath = "//div[@id='seoH1DontRemoveContainer']/h1")
    public WebElement properties;
    @FindBy(xpath = "//span[contains(text(),\"MMT Luxe Selections\")]")
    public WebElement mmtLuxeFilter;
    @FindBy(xpath = "//span[contains(text(),\"Taj Bengal, Kolkata\")]")
    public WebElement tajBengal;

    @FindBy(xpath = "//button[contains(text(),'BOOK THIS NOW')]")
    public WebElement bookThisNowBtn;



}

package pages.actions;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import pages.locators.SearchResultPageLocator;
import utils.BaseUtils;
import utils.DriverManager;

import java.util.ArrayList;
import java.util.List;

public class SearchResultPageActions {
    public static WebDriver driver;
    public static SearchResultPageLocator searchResultPageLocator;

    public SearchResultPageActions(WebDriver driver) {
        this.driver = driver;
        searchResultPageLocator = new SearchResultPageLocator();
        PageFactory.initElements(this.driver, searchResultPageLocator);

    }

    public static void verifySearchFilterAndProperies() throws InterruptedException {
       Thread.sleep(2000);
        if(searchResultPageLocator.filterField.isDisplayed()){
            DriverManager.log.info("--Search Filter is Displayed--");
        }else {
            DriverManager.log.error("--Search Filter is Not Displayed--");
        }
        BaseUtils.explicitWait(searchResultPageLocator.properties,driver);
        if (searchResultPageLocator.properties.isDisplayed()){
            DriverManager.log.info("--No Of Properties is Displayed--");
        }
        else {
            DriverManager.log.error("--No Of Properties is Not Displayed--");
        }
    }

    public static void selectMMTLuxe(){
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", searchResultPageLocator.mmtLuxeFilter);
        DriverManager.log.info("--MMTLuxe section is selected--");
    }
    public static void clickOnTajBengal() throws InterruptedException {
        Thread.sleep(3000);
        List<String> tab1= new ArrayList<>(driver.getWindowHandles());
        int noOfTab=tab1.size();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", searchResultPageLocator.tajBengal);
        DriverManager.log.info("--Sucessfully Clicked on Taj bengal--");
        List<String> tabs2 = new ArrayList<>(driver.getWindowHandles());
        int noOfTabAfterClickOnTheHotelBtn =tabs2.size();
        if((noOfTab+1) == noOfTabAfterClickOnTheHotelBtn){
           DriverManager.log.info("--Redirected to Taj Bengal Hotel----");
        } else{
            DriverManager.log.error("---Not able to redirect to Taj Bengal Hotel Page---");
        }
    }
    public static void verifyTajBengalBookingPage() throws InterruptedException {
        Thread.sleep(5000);
        List<String> tab1 = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tab1 .get(1));
        if(searchResultPageLocator.tajBengal.isDisplayed()){
            DriverManager.log.info("--Taj Bengal Hotel Option is present--");
        }else{
            DriverManager.log.error("--Taj Bengal Hotel Option is not Visible--");
        }

    }

    public static void verifyBookThisNowButton(){
       if (searchResultPageLocator.bookThisNowBtn.isDisplayed()){
           DriverManager.log.info("--Book now button is displayed--");
       }
       else {
           DriverManager.log.error("--Book now button is not Displayed--");
       }
    }


}

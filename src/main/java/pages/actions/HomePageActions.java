package pages.actions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import pages.locators.HomePageLocators;

public class HomePageActions {
    public static WebDriver driver;
    public static HomePageLocators homePageLocators;

    public HomePageActions(WebDriver driver) {
        this.driver = driver;
        homePageLocators = new HomePageLocators();
        PageFactory.initElements(this.driver, homePageLocators);

    }
    public static void getHomePageTitleTest(){
        System.out.println("HomePage Title is: "+driver.getTitle());

    }
    public static boolean isFlightBannerPresent() {
        return homePageLocators.flightBanner.isDisplayed();
    }
    public static boolean isHotelsBannerPresent() {
        return homePageLocators.hotelsBanner.isDisplayed();
    }
    public static boolean isHomestaysPresent() {
        return homePageLocators.homeStaysBanner.isDisplayed();
    }
    public static void navigateToHotelsPage(){
        homePageLocators.hotelsBanner.click();

    }


}

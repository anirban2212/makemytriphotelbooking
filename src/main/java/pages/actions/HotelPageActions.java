package pages.actions;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import pages.locators.HotelPageLocators;
import utils.BaseUtils;
import utils.DriverManager;


public class HotelPageActions {
    public static WebDriver driver;
    public static HotelPageLocators hotelPageLocators;

    public HotelPageActions(WebDriver driver) {
        this.driver = driver;
        hotelPageLocators = new HotelPageLocators();
        PageFactory.initElements(this.driver, hotelPageLocators);

    }

    public static String getHotelPageTitle() {
        return driver.getTitle();

    }

    public static void selectCity(String cityName) throws InterruptedException {
        hotelPageLocators.cityField.click();
        BaseUtils.explicitWait(hotelPageLocators.cityTextBox, driver);
        hotelPageLocators.cityTextBox.sendKeys(cityName);
        BaseUtils.explicitWait(hotelPageLocators.firstSuggesation, driver);
        Thread.sleep(3000);
        hotelPageLocators.firstSuggesation.click();


    }

    public static void selectCheckInDate() {
        String month = "December";
        String date = "23";
        String year = "2023";

        while (true) {
            BaseUtils.explicitWait(hotelPageLocators.monthYear, driver);
            String monthYearYear = hotelPageLocators.monthYear.getText();

            String arr[] = monthYearYear.split("2");
            String mon = arr[0];

            String yr = hotelPageLocators.year.getText();


            if (mon.equalsIgnoreCase(month) && yr.equalsIgnoreCase(year)) {
                break;
            } else {
                hotelPageLocators.nextBtnCal.click();
            }

        }
        /*
        Date selection
         */

        for (WebElement ele : hotelPageLocators.checkInDate) {
            String dt = ele.getText();
            if (dt.equals(date)) {
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", ele);
                break;
            }

        }
    }

    public static void selectCheckOutDate() {
        String month = "December";
        String date = "26";
        String year = "2023";

        while (true) {
            BaseUtils.explicitWait(hotelPageLocators.monthYear, driver);
            String monthYearYear = hotelPageLocators.monthYear.getText();
            String arr[] = monthYearYear.split("2");
            String mon = arr[0];

            String yr = hotelPageLocators.year.getText();

            if (mon.equalsIgnoreCase(month) && yr.equalsIgnoreCase(year)) {
                break;
            } else {
                hotelPageLocators.nextBtnCal.click();
            }

        }
        /*
        Date selection
         */

        for (WebElement ele : hotelPageLocators.checkOutDate) {
            String dt = ele.getText();
            if (dt.equals(date)) {
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", ele);

                break;
            }

        }


    }

    public static void selectRoom() {

        hotelPageLocators.roomsBtn.click();
        hotelPageLocators.roomNo.click();

    }

    public static void selectNoOfAdults() {
        hotelPageLocators.adultsBtn.click();
        hotelPageLocators.noOfAdults.click();
    }

    public static void selectNoOfChildren() {
        hotelPageLocators.childernBtn.click();
        hotelPageLocators.noOfChild.click();
    }

    public static void selectChildAge() {
        hotelPageLocators.childBtn.click();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", hotelPageLocators.childAge);
        DriverManager.log.info("--child age selected successfully--");
    }

    public static void clickOnApplyBtnAndSearch() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", hotelPageLocators.applyBtn);
        BaseUtils.explicitWaitClick(hotelPageLocators.searchBtn, driver);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", hotelPageLocators.searchBtn);
        DriverManager.log.info("--clicked on Apply Button & Searched for the results--");
    }
}





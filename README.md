This project is used to validate the MakeMyTrip website.
Prerequisites for this project::
	1.	JDK(version 11 is preferable)
	2.	Maven
	3.	IDE(Eclipse,Intellij)
	4.	The cucumber plugin should be installed in the project.
	5.	TestNG plugin should be installed in the project.
Steps to execute the project:
	1.	Take a pull of the codebase from Git Hub.
	2.	Build the project to download the maven dependencies for the project.
	3.	Execute the TestRunner.java file under src/test/java/testRunner file to execute the existing feature files.
Check the test-output folder for the spark and html report.
**Note::Last execution report is attached in the test-output folder.

